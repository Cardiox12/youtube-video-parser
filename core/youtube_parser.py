#!/usr/bin/env python3 

from pprint import pprint
from bs4 import BeautifulSoup
import requests 
import re

class YoutubeVideoParser(object):
    def __init__(self, url):
        self.url = url 
        self.subtitles_dict = {}
        self.parsed_subtitles = []
        self.languages = ["fr", "ja", "en", "ru", 
                          "de", "pt", "et", "ko", "es"]

    def get_video_id(self):
        """
            Get the youtube video id

            :returns: the youtube video id
            :rtype: str
        """
        if re.search(r'(w{3}.youtube.com)', self.url):
            return re.findall(r'v=(.{11})', self.url)[0]
        else:
            raise Exception("Link needs to be youtube link")

    def get_subtitles(self):
        """
            Get the subtitles for all languages from the youtube video

            :returns: a dict with found subtitles, if no subtitles found, raise an Exception 
            :rtype: dict
        """ 
        video_id = self.get_video_id(self.url)

        for lang in self.languages:
            url_template = "https://video.google.com/timedtext?lang={0}&v={1}&caps=asr".format(lang, video_id)
            req = requests.get(url_template)

            if req.status_code == 200:
                subtitles = req.text
                if subtitles:
                    self.subtitles_dict[lang] = subtitles
        if not self.subtitles_dict:
            raise Exception("It appears the video has no subtitles or cannot be parsed")

        return self.subtitles_dict


    def format_subtitles(self, lang):
        """
            Format the subtitles for the selected languages

            :param lang: the subtitle language you want to parse
            :type lang: str
            :returns: a list containing tuples structured with (start, dur, subtitle)
            :rtype: list
        """
        sub_lang = self.subtitles_dict.get(lang)
        tmp = []
        if sub_lang is not None:
            parser = BeautifulSoup(sub_lang, 'html.parser')
            for subtitle in  parser.find_all("text"):
                tmp.append([
                        subtitle.attrs.get('start'),
                        subtitle.attrs.get('dur'),
                        subtitle.text.lower().strip().replace('\n', ' ')
                    ])
        else:
            raise Exception("No subtitle found for the selected language") 

        self.parsed_subtitles = tmp
        return self.parsed_subtitles


    def _eval_timestamp(self, to_find, subtitles, timestamp, duration):
        """
            Evaluate the real timestamp of to_find

            :param to_find: the word to find (needle)
            :type to_find: str
            :param subtitles: the subtitles where to find to_find (haystack)
            :type subtitles: list
            :param timestamp: the timestamp of subtitles
            :type timestamp: float
            :param duration: the duration of subtitles
            :type duration: float
            :returns: returns the exact timestamp of to_find
            :rtype: float
        """
        base = duration / len(subtitles)
        return timestamp + base * subtitles.index(to_find)


    def find_occurence(self, to_find, lang):
        """
            Find the occurence of to_find in subtitles

            :param to_find: the word to_find
            :type to_find: str
            :param lang: the language which find to_find
            :type lang: str
            :returns: returns list of occurences
            :rtype: list
        """
        occurences = []
        self.get_subtitles()
        for f_subtitle in self.format_subtitles(lang):
            subtitles = f_subtitle[2]
            if to_find in subtitles:
                timestamp = f_subtitle[0]
                duration = f_subtitle[1]
                occurences.append((
                        self._eval_timestamp(to_find, 
                                             subtitles, 
                                             float(timestamp), 
                                             float(duration)),
                        subtitles
                    ))
        pprint(occurences)


if __name__ == "__main__":
    yvp = YoutubeVideoParser("https://www.youtube.com/watch?v=HEfHFsfGXjs&t")
    yvp.find_occurence("croquet", "en")
